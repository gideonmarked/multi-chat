<!DOCTYPE html>
<html>
<head>
	<title>Multi Chat App</title>
	<link href="style.css" rel="stylesheet">
	<style type="text/css">
		.ico-videochat {
		    background: url(http://localhost/wc-templates/assets/ico-videochat.png) no-repeat;
		    width: 24px;
		    height: 21px;
		}
		.ico-visit {
		    background: url(http://localhost/wc-templates/assets/ico-visit.png) no-repeat;
		    width: 19px;
		    height: 22px;
		}
		.ico-arrow-right-blue {
		    background: url(http://localhost/wc-templates/assets/ico-arrow-right-blue.png) no-repeat;
		    width: 24px;
		    height: 27px;
		}
		div#video-list {
		    position: absolute;
		    top: 35px;
	        left: 4px;
	        z-index: 2;
		}
		.chat-messages-top {
		    position: relative;
		}

div#video-list a {
text-transform: uppercase;
    text-decoration: none;
    background: #fff;
    padding: 3px;
    margin-right: 1px;
    border: 1px solid #ec008c;
}
	</style>
</head>
<body>
	<div id="root"></div>
	<input type="hidden" id="converter" value="345" >
	<!-- Load Babel -->
	<script src="jquery-3.2.1.min.js"></script>
	<script src="select2.full.min.js"></script>
	<script src="src/client/public/multi_chat.js"></script>
	<script src="script.js"></script>
</body>
</html>