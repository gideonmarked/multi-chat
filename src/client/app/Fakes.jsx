import React from 'react';
import Fake from './Fake.jsx';
import config from '../config/index.jsx';

class Fakes extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			fakes: this.props.fakes,//this.props.users,
			owner: this.props.owner
		};
		this.updateClickHandler = this.updateClickHandler.bind(this);
		this.showFakeList = this.showFakeList.bind(this);
	}

	componentWillReceiveProps(props){
		if( props.fakes ) {
			this.setState({
				fakes: props.fakes
			});
		}
	}

	updateClickHandler( user )
	{
		this.props.onUpdateSender( user );
	}

	showFakeList(event) {
		this.props.onUpdateSender( null );
		event.preventDefault();
	}

	render() {
		if ( this.state.fakes.length == 0 ) {
			return ( <nav className="nav-inner nav-inner-bottom">
						<ul>
							<li className="active join-as-fake" key={ 1000 }>
								<a href="#" onClick={ this.showFakeList }>
									Join as Fake User
								</a>
							</li>
						</ul>
					 </nav>
					);
		}

		var fakes = this.state.fakes.map((user, i) => {
			return (
            	<Fake
				key={ i }
				user={ user }
				onClickUpdate={ this.updateClickHandler }
				 />
			);
	    });

	    fakes = [...fakes, <li className="active join-as-fake" key={ 1000 }><a href="#" onClick={ this.showFakeList }>Join as Fake User</a></li>];

	    return (
	    	<nav className="nav-inner nav-inner-bottom">
	            <ul>
	            	{ fakes }
	            </ul>
	        </nav>
	    );
	}
}

export default Fakes;