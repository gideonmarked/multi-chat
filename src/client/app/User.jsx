import React from 'react';
import ReactDOM from 'react-dom';

class User extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			user: this.props.user,
		};
		this.clickHandler = this.clickHandler.bind(this);
	}

	clickHandler() {
		this.props.onClickUpdate( this.state.user );
	}

	render(){

		var pkg = 'Basic';
		switch( this.state.user.package_id ) {
			case 1:
				pkg = 'basic';
			break;

			case 2:
				pkg = 'plus';
			break;

			case 3:
				pkg = 'platinum';
			break;

			case 4:
				pkg = 'vip';
			break;
		}

		var img = '';

		if( this.state.user.image == null || this.state.user.image == 'null' || this.state.user.image == '' ) {
			if( this.state.user.sex == 'male' ) {
				img = 'http://www.betset.io/public//uploads/users/profile_m.png';
			} else {
				img = 'http://www.betset.io/public//uploads/users/profile_f.png';
			}
		} else {
			img = 'http://www.betset.io/public//uploads/users/' + this.state.user.image;
		}

		return (
			<div className="chat-user-side">
	            <a href="#">
	            	<img src={ img } width="29" height="28" />
	            </a>
	            <div className="user-details">
	                <div className="user-detail">
	                    <span className="username">{ this.state.user.nickname }</span>
	                    <span className={"type type-" + pkg} style={{ textTransform: 'uppercase' }}>{ pkg }</span>
	                    <a href="#" onClick={ this.clickHandler } className="btn btn-xs">Start Chat</a>
	                </div>
	                <div className="user-status">
	                    <div className="status status-online"></div>
	                    { this.state.user.age }, { this.state.user.sex } / this.state.user.country_id<br />
	                    <strong>Join</strong>: { this.state.user.created_at }
	                </div>
	            </div>
	        </div>
		);
	}
}

export default User;