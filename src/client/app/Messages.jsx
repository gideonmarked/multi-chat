import React from 'react';

import Message from './Message.jsx';

class Messages extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      owner: this.props.owner,
    };
  }

  componentDidUpdate() {
    // There is a new message in the state, scroll to bottom of list
    const objDiv = document.getElementById('messageList');
    objDiv.scrollTop = objDiv.scrollHeight;
  }

  render() {
    // Loop through all the messages in the state and create a Message component
    const messages = this.props.messages.map((message, i) => {
        if( (this.state.owner.email == message.sender.email) ) {
          message.fromMe = true;
        }

        if( message.fromMe || (this.state.owner.email == message.receiver.email) ){
          return (
            <Message
              key={i}
              username={message.username}
              message={message.message}
              fromMe={message.fromMe} />
          );
        }
      });

    return (
      <div className='messages' id='messageList'>
        { messages }
      </div>
    );
  }
}

Messages.defaultProps = {
  messages: [],
  owner: 'Anonymous'
};

export default Messages;