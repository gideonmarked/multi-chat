import React from 'react';

class Message extends React.Component {
  render() {
    // Was the message sent by the current user. If so, add a css class
    const fromMe = this.props.fromMe ? 'from-me' : '';
    return (
      <div className={`message ${( fromMe ? 'message-right' : 'message-left' )}`}>
          { this.props.message }
      </div>
    );
  }
}

Message.defaultProps = {
  message: '',
  username: '',
  owner: '',
  fromMe: false
};

export default Message;