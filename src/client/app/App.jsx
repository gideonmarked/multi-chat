import React from 'react';
import ReactDOM from 'react-dom';
import ChatApp from './ChatApp.jsx';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      converter: document.getElementById('converter').value
    };
  }

  render() {
      return (
        <ChatApp converter={ this.state.converter }/>
      );
  }

}
App.defaultProps = {
};

ReactDOM.render(<App/>, document.getElementById('root'));