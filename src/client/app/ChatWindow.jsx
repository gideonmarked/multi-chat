import React from 'react';
import io from 'socket.io-client';
import config from '../config/index.jsx';

import Messages from './Messages.jsx';
import ChatInput from './ChatInput.jsx';

class ChatWindow extends React.Component {
	constructor(props) {
	    super(props);
	    this.state = {
	    	messages: this.props.messages,
	    	socket: this.props.socket,
	    	peer: this.props.peer,
	    	show_video_list: false,
	    	chatwindow: this.props.chatwindow
	    };

	    var _this = this;

	    this.sendHandler = this.sendHandler.bind(this);
	    this.onReceiveCall = this.onReceiveCall.bind(this);
	    this.onReceiveStream = this.onReceiveStream.bind(this);
	    this.getVideo = this.getVideo.bind(this);
	    this.toggleVideoList = this.toggleVideoList.bind(this);
	    this.selectVideo1 = this.selectVideo1.bind(this);
	    this.selectVideo2 = this.selectVideo2.bind(this);
	    this.selectVideo3 = this.selectVideo3.bind(this);

	    this.conn = this.state.peer.connect(_this.props.chatwindow[1].id, {metadata: {
            'username': _this.props.chatwindow[1].nickname
        }});
	      
	    console.log('generated', this.state.peer.id);

	    this.state.peer.on('call', function(call) {
	    	console.log('answering call..');
	    	_this.onReceiveCall(call);
		});
	}

	componentWillReceiveProps( props ) {

		if( props.socket != this.state.socket ) {
			this.setState({
		    	socket: props.socket
		    });
		}

		this.setState(
			{
				messages:props.messages,
				chatwindow:props.chatwindow
			});
	}

	onReceiveCall(call){
		var _this = this;
	    call.answer(window.localStream);
	    call.on('stream', function(stream){
	      window.peer_stream = stream;
	      _this.onReceiveStream(stream, 'video-received');
	    });
	}

	onReceiveStream(stream, element_id){
	    var video = document.getElementById(element_id).getElementsByTagName('video')[0];
	    video.src = window.URL.createObjectURL(stream);
	    window.peer_stream = stream;
	}

	sendHandler(message) {
	    const messageObject = {
	      sender: this.state.chatwindow[0],
	      receiver: this.state.chatwindow[1],
	      message
	    };

	    messageObject.timestamp = Date.now();
	    messageObject.fromMe = false;

	    // Emit the message to the server
	    this.state.socket.emit('client:message', messageObject);

	    messageObject.fromMe = true;

	    //sends messages to some user
	    this.props.updateMessages(messageObject);
	}

	addMessage(message) {
	    // Append the message to the component state
	    const messages = this.state.messages;
	    messages.push(message);
	    this.setState({ messages });
	}

	getVideo(callback){
	    navigator.getUserMedia({audio: false, video: true}, callback, function(error){
	      console.log(error);
	      alert('An error occured. Please try again');
	    });
	}

	toggleVideoList(event) {
		this.setState({
			show_video_list: !this.state.show_video_list
		});

		event.preventDefault();
	}

	selectVideo1(event) {
		const messageObject = {
	      sender: this.state.chatwindow[0],
	      receiver: this.state.chatwindow[1],
	      type: 'video',
	      url: 'http://localhost/peerclient1/videos/1.mp4'
	    };

	    messageObject.timestamp = Date.now();
	    messageObject.fromMe = false;

	    // Emit the message to the server
	    this.state.socket.emit('client:message', messageObject);

	    messageObject.fromMe = true;

	    this.playVideo( messageObject.url );
	    this.toggleVideoList(event);

		event.preventDefault();
	}

	selectVideo2(event) {
		const messageObject = {
	      sender: this.state.chatwindow[0],
	      receiver: this.state.chatwindow[1],
	      type: 'video',
	      url: 'http://localhost/peerclient1/videos/2.mp4'
	    };

	    messageObject.timestamp = Date.now();
	    messageObject.fromMe = false;

	    // Emit the message to the server
	    this.state.socket.emit('client:message', messageObject);

	    messageObject.fromMe = true;

	    this.playVideo( messageObject.url );
	    this.toggleVideoList(event);

		event.preventDefault();
	}

	selectVideo3(event) {
		const messageObject = {
	      sender: this.state.chatwindow[0],
	      receiver: this.state.chatwindow[1],
	      type: 'video',
	      url: 'http://localhost/peerclient1/videos/3.mp4'
	    };

	    messageObject.timestamp = Date.now();
	    messageObject.fromMe = false;

	    // Emit the message to the server
	    this.state.socket.emit('client:message', messageObject);

	    messageObject.fromMe = true;

	    this.playVideo( messageObject.url );
	    this.toggleVideoList(event);

		event.preventDefault();
	}

	playVideo( new_url ) {
		var xhr = new XMLHttpRequest();
        xhr.responseType = 'blob';

        xhr.onload = function() {

            var reader = new FileReader();
            
            reader.onloadend = function() {
            
            var byteCharacters = atob(reader.result.slice(reader.result.indexOf(',') + 1));

            var byteNumbers = new Array(byteCharacters.length);

            for (var i = 0; i < byteCharacters.length; i++) {

            byteNumbers[i] = byteCharacters.charCodeAt(i);

            }

            var byteArray = new Uint8Array(byteNumbers);
            var blob = new Blob([byteArray], {type: 'video/MP4'});
            var url = URL.createObjectURL(blob);

            document.getElementById('video-sent').src = url;
          
        }
        
        reader.readAsDataURL(xhr.response);
        
      };

      xhr.open('GET', new_url);
      xhr.send();
	}

	render() {
		if( !this.state.chatwindow) {
			return (
				<div>No User for this Window</div>
			);
		}

		var video_list = null;

		if( this.state.show_video_list ) {
			video_list = (
				<div id="video-list">
					<a href="#" onClick={ this.selectVideo1 }>Video 1</a>
					<a href="#" onClick={ this.selectVideo2 }>Video 2</a>
					<a href="#" onClick={ this.selectVideo3 }>Video 3</a>
				</div>
			);
		}

	    return (
            <div className="chat-messages">
                <div className="chat-messages-top">
                    <span className="user">{ this.state.chatwindow[0].nickname }</span>
                    <a href="#" onClick={ this.toggleVideoList }><span className="ico ico-videochat"></span></a>
                    <span className="user">{ this.state.chatwindow[1].nickname }</span>
                    <a href="#"><span className="ico ico-last ico-visit"></span></a>
                    { video_list }
                </div>
                <div className="chat-messages-inner">
                	<div id="video-received" >
                		<video width="95" height="73" muted autoPlay style={{ float: 'left' }} />
                	</div>

                	<div>
                		<video  id="video-sent" width="95" muted height="73" autoPlay style={{ float: 'left' }} />
                	</div>

                    <div className="timestamp">10.02.2017, 19.18</div>
                    <div className="profile-pic-left"> 
                       <img src="images/pic-profile-small-1.jpg" alt="" width="29" height="28" />
                    </div>
                    <Messages messages={this.state.messages} owner={ this.state.chatwindow[0] } />
                </div>
                <div className="chat-message-send">
                    <ChatInput onSend={this.sendHandler} />
                </div>
            </div>
	    );
	}
}

ChatWindow.defaultProps = {
	  username: 'Anonymous',
	  recepient: 'Anonymous'
};

export default ChatWindow;