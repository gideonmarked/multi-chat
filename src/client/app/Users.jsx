import React from 'react';
import ReactDOM from 'react-dom';
import config from '../config/index.jsx';

import User from './User.jsx';

class Users extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			people: [],//this.props.users,
			converter: this.props.converter
		};
		this.updateClickHandler = this.updateClickHandler.bind(this);
	}

	updateClickHandler( user ) {
		this.props.onUpdateReceiver( user );
	}

	componentDidMount(){
		fetch(config.chat_people + this.props.converter)
		.then( results => {
			return results.json();
		})
		.then( data => {
	      var people = data.map( function(user,i) {
	        return user;
	      });
	      this.setState({people: people});
	    });
	}

	render(){
		if( !this.state.people ) {
			return (
				<div className="chat-section">
					<div className="chat-wide">
					</div>
				</div>
			);
		}

		var users = this.state.people.map((user,i)=>{
			return (
				<User key={i} user={ user } onClickUpdate={ this.updateClickHandler } />
			);
		});

		return (
			<div className="chat-section">
				<div className="chat-wide">
					{ users }
		        </div>
		        <nav aria-label="Page navigation" className="chat-navigation">
	              <ul className="pagination">
	                  <li><a href="#">1</a></li>
	                  <li>
	                      <a href="#" aria-label="Next">
	                          Next
	                          <span aria-hidden="true">»</span>
	                      </a>
	                  </li>
	              </ul>
	            </nav>
            </div>
		);
	}
}

export default Users;