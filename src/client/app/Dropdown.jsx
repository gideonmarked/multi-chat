import React from 'react';

class Dropdown extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			label: this.props.label,
			options: this.props.options
		}
	}

	render() {
		var options = null;
		return (
			<div className="form-group">
	            <label htmlFor={"profile-" + this.state.label.toLowerCase()}>{ this.state.label }</label>
	            <select name={"profile-" + this.state.label.toLowerCase()} id={"profile-" + this.state.label.toLowerCase()} className="form-control select2-hidden-accessible" tabIndex="-1" aria-hidden="true">
	                
	            </select>
	            <span className="select2 select2-container select2-container--default" dir="ltr" style={{ width: 90+'px' }}>
	            	<span className="selection"><span className="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" tabIndex="0" aria-labelledby="select2-profile-gender-container">
	            		<span className="select2-selection__rendered" id="select2-profile-gender-container" title="Male">Male</span><span className="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span className="dropdown-wrapper" aria-hidden="true"></span></span>
	        </div>
		);
	}
}

export default Dropdown;