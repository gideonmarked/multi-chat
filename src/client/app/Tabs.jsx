import React, { Component } from 'react';

import FakeList from './FakeList.jsx';
import Tab from './Tab.jsx';

class Tabs extends Component {
	constructor(props) {
		super(props);
		this.state = {
			messages: this.props.messages,
			chatwindows: this.props.chatwindows,
			sender: this.props.sender,
			socket: this.props.socket
		};
		this.selectFake = this.selectFake.bind(this);
		this.updateMessages = this.updateMessages.bind( this );
	}

	componentWillReceiveProps( props ) {
		this.setState({
			messages: props.messages,
			chatwindows: props.chatwindows,
			sender: props.sender,
			socket: props.socket
		});
	}

	updateMessages( message ) {
		this.props.updateMessages( message );
	}

	selectFake( fake_user ) {
		this.props.addFakeUser( fake_user );
	}

	render () {
		var tabs = null;
		if( this.state.sender == null || this.state.chatwindows.length  == 0 ) {
				tabs = <FakeList selectFakeUser={ this.selectFake } active={ true } />;
		} else {
			if( this.state.chatwindows.length > 0 ) {
				var chatwindows = [];
				this.state.chatwindows.map((chatwindow,i)=>{
					if( chatwindow[0].email == this.state.sender.email ) {
						chatwindows.push(chatwindow);
					}
				});

				if( chatwindows.length > 0 ) {
					return (<Tab
							chatwindows={ chatwindows }
							sender={ this.state.sender }
							socket={ this.state.socket }
							messages={ this.state.messages }
							updateMessages={ this.updateMessages }
							/>);
				} else {
					return null;
				}

			} else {
				tabs = <FakeList selectFakeUser={ this.selectFake } active={ true } />;
			}
		}
		return (
			<div id="tabs">{ tabs }</div>
		);
	}
}

export default Tabs;