import React from 'react';
import ReactDOM from 'react-dom';

import ChatWindow from './ChatWindow.jsx';

class ChatWindows extends React.Component {
	constructor(props) {
	    super(props);
	    this.state = {
	    	chatwindows: [],
	    };
	    this.updateMessages = this.updateMessages.bind(this);
	}

	updateMessages( message ) {
		this.props.updateMessages( message );
	}

	render(){
		var chatwindows = this.props.chatwindows.map((chatwindow,i)=>{
			return (
				<ChatWindow key={i} chatwindow={ chatwindow } updateMessages={ this.updateMessages } />
			);
		});
		return (
			<div className="chat-container-bottom">
            	{ chatwindows }
          	</div>
		);
	}
}

export default ChatWindows;