import React from 'react';

class Fake extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			user: this.props.user
		};
		this.clickHandler = this.clickHandler.bind(this);
	}

	clickHandler(event) {
	    // Stop the form from refreshing the page on submit
	    event.preventDefault();

	    // Call the onSend callback with the chatInput message
	    this.props.onClickUpdate( this.state.user );
	}

	render() {
		return (
			<li>
                <a href="#" onClick={ this.clickHandler }>{ this.state.user.nickname }</a>
            </li>
		);
	}
}

export default Fake;