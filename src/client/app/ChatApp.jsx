import React from 'react';
import io from 'socket.io-client';
import config from '../config/index.jsx';

import Tabs from './Tabs.jsx';
import Search from './Search.jsx';
import Fakes from './Fakes.jsx';
import Users from './Users.jsx';

class ChatApp extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      converter: this.props.converter,
      sender: null,
      owner: null,
      receiver: [],
      fakes: [],
      messages: [],
      chatwindows: []
    };
    
    this.updateReceiver = this.updateReceiver.bind(this);
    this.updateSender = this.updateSender.bind(this);
    this.addFakeUser = this.addFakeUser.bind(this);
    this.addMessage = this.addMessage.bind(this);
    this.updateMessages = this.updateMessages.bind( this );

    
  }

  componentDidMount() {
    fetch(config.chat_converter + this.state.converter)
    .then( results => {
      return results.json();
    })
    .then( data => {
      const email = data.email;
      this.socket = io(config.server, { query: `username=${email}` }).connect();
      
      this.socket.on('server:message', message => {
        this.addMessage(message);
      });

      this.socket.on('server:saved_message', message => {
        this.addMessage(message);
      });

      this.setState({
        owner: data,
        socket: this.socket
      });
    });
  }

  addMessage(message) {
      // Append the message to the component state
      const messages = this.state.messages;
      messages.push(message);
      this.setState({ messages });
  }

  updateReceiver( user ){
    if(!this.state.sender) {
      alert('No fake user selected');
    } else {

      var match = this.state.chatwindows.map((pair,i)=>{
        if( this.state.sender == pair[0] && user == pair[1] ) {
          return true;
        } else {
          return false;
        }
      });
      

      if( !match[0] ) {
        this.setState(
          {
            chatwindows: [...this.state.chatwindows, [this.state.sender,user]]
          }
        );
      }
    }
  }

  updateSender( user ){
    this.setState({sender:user});
  }

  addFakeUser( fake_user ) {
    var match = false;
    this.state.fakes.map((fake)=>{
      if( fake_user.email == fake.email ) {
        match = true;
      }
    });

    if( !match ) {
      this.setState({
        sender: fake_user,
        fakes: [...this.state.fakes,fake_user]
      });
    }
  }

  updateMessages( message ) {
    this.addMessage( message );
  }

  render() {
    if( !this.state.owner) {
      return (
        <div> No Convertor Loaded. </div>
      );
    }

    return (
      <div className="content">
        <div className="content-top">
            <a href="#" className="ico ico-exit">Exit</a>
        </div>
        <div className="wrapper">
          <nav className="nav-inner">
              <ul>
                  <li><a href="#">Online Members</a></li>
                  <li><a href="#">Unallocated Members</a></li>
              </ul>
          </nav>

          <Search />

          <Users onUpdateReceiver={ this.updateReceiver } converter={ this.state.converter } />

          <Fakes onUpdateSender={ this.updateSender } fakes={ this.state.fakes } />
          
          <Tabs
          addFakeUser={ this.addFakeUser }
          chatwindows={ this.state.chatwindows }
          messages={ this.state.messages }
          socket={ this.state.socket }
          peer={ this.state.peer }
          sender={ this.state.sender }
          updateMessages={ this.updateMessages }
          />

        </div>
      </div>
    );
  }

}

ChatApp.defaultProps = {
  username: 'Anonymous'
};

export default ChatApp;