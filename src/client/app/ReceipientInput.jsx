import React from 'react';

class RecepientInput extends React.Component {
  constructor(props) {
    super(props);
    this.state = { receipientInput: '' };

    // React ES6 does not bind 'this' to event handlers by default
    this.submitHandler = this.submitHandler.bind(this);
    this.textChangeHandler = this.textChangeHandler.bind(this);
  }
  
  submitHandler(event) {
    // Stop the form from refreshing the page on submit
    event.preventDefault();

    // Clear the input box
    this.setState({ receipientInput: '' });

    // Call the onSend callback with the chatInput message
    this.props.onSend(this.state.receipientInput);
  }

  textChangeHandler(event)  {
    this.setState({ receipientInput: event.target.value });
  }

  render() {
    return (
      <form className="chat-input" onSubmit={this.submitHandler}>
        <label>Add Chatter</label>
        <input type="text"
          onChange={this.textChangeHandler}
          value={this.state.receipientInput}
          placeholder="Write a message..."
          required />
      </form>
    );
  }
}

RecepientInput.defaultProps = {
};

export default RecepientInput;