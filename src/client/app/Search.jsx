import React from 'react';
import ReactDOM from 'react-dom';
import Dropdown from './Dropdown.jsx';

class Search extends React.Component {
	constructor(props) {
		super(props);
        this.state = {
            view: false
        };
        this.toggleView = this.toggleView.bind(this);
	}

    toggleView(event){
        this.setState({
            view: !this.state.view
        });
        event.preventDefault();
    }

	render() {
        var form = null;
        if( this.state.view ) {
            form = (
                <form>
                    <div className="profiles">
                        <div className="label">Profiles</div>

                        <label htmlFor="profile-with-image" className="label-inline">
                            <input type="checkbox" name="profile-with-image" id="profile-with-image" className="form-control" />
                            With image
                        </label>

                        <label htmlFor="profile-online" className="label-inline">
                            <input type="checkbox" name="profile-online" id="profile-online" className="form-control" />
                            Online
                        </label>

                        <label htmlFor="profile-camera" className="label-inline">
                            <input type="checkbox" name="profile-camera" id="profile-camera" className="form-control" />
                            Camera
                        </label>
                    </div>
                    <div className="members-right">
                        <div className="members-top">
                            <div className="form-group">
                                <label htmlFor="profile-name">Name</label>
                                <input type="text" name="profile-name" id="profile-name" placeholder="Name" className="form-control" />
                            </div>

                            <Dropdown label="Gender" />

                            <div className="form-group">
                                <label htmlFor="profile-myusers">My Users</label>
                                <select name="profile-myusers" id="profile-myusers" className="form-control select2-hidden-accessible" tabIndex="-1" aria-hidden="true">
                                    <option value="">Users allocated to me</option>
                                </select><span className="select2 select2-container select2-container--default" dir="ltr" style={{ width: 174+'px' }}><span className="selection"><span className="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" tabIndex="0" aria-labelledby="select2-profile-myusers-container"><span className="select2-selection__rendered" id="select2-profile-myusers-container" title="Users allocated to me">Users allocated to me</span><span className="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span className="dropdown-wrapper" aria-hidden="true"></span></span>
                            </div>
                        </div>
                        <div className="members-bottom">
                            <div className="form-group">
                                <label htmlFor="profile-language">Language</label>
                                <select name="profile-language" id="profile-language" className="form-control select2-hidden-accessible" tabIndex="-1" aria-hidden="true">
                                    <option value="">Any</option>
                                </select><span className="select2 select2-container select2-container--default" dir="ltr" style={{ width: 90+'px' }}><span className="selection"><span className="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" tabIndex="0" aria-labelledby="select2-profile-language-container"><span className="select2-selection__rendered" id="select2-profile-language-container" title="Any">Any</span><span className="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span className="dropdown-wrapper" aria-hidden="true"></span></span>
                            </div>
                            <div className="form-group">
                                <label htmlFor="profile-country">Country</label>
                                <select name="profile-country" id="profile-country" className="form-control select2-hidden-accessible" tabIndex="-1" aria-hidden="true">
                                    <option value="">Any</option>
                                </select><span className="select2 select2-container select2-container--default" dir="ltr" style={{ width: 90+'px' }}><span className="selection"><span className="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" tabIndex="0" aria-labelledby="select2-profile-country-container"><span className="select2-selection__rendered" id="select2-profile-country-container" title="Any">Any</span><span className="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span className="dropdown-wrapper" aria-hidden="true"></span></span>
                            </div>
                            <div className="form-group">
                                <label htmlFor="profile-membership">Membership</label>
                                <select name="profile-membership" id="profile-membership" className="form-control select2-hidden-accessible" tabIndex="-1" aria-hidden="true">
                                    <option value="">Any</option>
                                </select><span className="select2 select2-container select2-container--default" dir="ltr" style={{ width: 90+'px' }}><span className="selection"><span className="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" tabIndex="0" aria-labelledby="select2-profile-membership-container"><span className="select2-selection__rendered" id="select2-profile-membership-container" title="Any">Any</span><span className="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span className="dropdown-wrapper" aria-hidden="true"></span></span>
                            </div>
                            <div className="form-group">
                                <button type="submit" className="btn btn-search">Search</button>
                            </div>
                        </div>
                    </div>
                </form>
            );
        }
		return (
			<div className="members-search">
                { form }
                <a href="#" onClick={ this.toggleView } className="btn btn-toggle toggle-members">Toggle Members Search</a>
            </div>
		);
	}
}

export default Search;