import React from 'react';
import config from '../config/index.jsx';

class FakeList extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			fakes: null,
			active: this.props.active
		};

		fetch(config.chat_fakes)
		.then( results => {
			return results.json();
		})
		.then( data => {
	      var fakes = data.map( function(user,i) {
	        return user;
	      });
	      this.setState({fakes: fakes});
	    });

	    this.clickHandler = this.clickHandler.bind(this);
	}

	clickHandler(event){
		var fake_user = null;
		this.state.fakes.map((fake)=>{
			if( fake.email == event.target.getAttribute('data-user') ) {
				fake_user = fake;
			}
		});
		if( fake_user ) {
			this.props.selectFakeUser(fake_user);
		}
		event.preventDefault();
	}

	render() {
		var fakes = null;
		if( !this.state.fakes ) {
			fakes = <div>No Fakes.</div>;
		} else if( !this.state.active ) {
			fakes = <div />;
		} else {
			fakes = this.state.fakes.map((fake,i)=>{

				var pkg = 'Basic';
				switch( fake.package_id ) {
					case 1:
						pkg = 'basic';
					break;

					case 2:
						pkg = 'plus';
					break;

					case 3:
						pkg = 'platinum';
					break;

					case 4:
						pkg = 'vip';
					break;
				}

				var img = '';

				if( fake.image == null || fake.image == 'null' || fake.image == '' ) {
					if( fake.sex == 'male' ) {
						img = 'http://www.betset.io/public//uploads/users/profile_m.png';
					} else {
						img = 'http://www.betset.io/public//uploads/users/profile_f.png';
					}
				} else {
					img = 'http://www.betset.io/public//uploads/users/' + fake.image;
				}

				return (
				<div className="chat-user-side" key={ i }>
                    <a href="#">
                    	<img src={ img } alt="" width="29" height="28" />
                    </a>
                    <div className="user-details">
                        <div className="user-detail">
                            <span className="username">{ fake.nickname }</span>
                            <span className={"type type-"+ { pkg }} style={{ textTransform: 'uppercase' }}>{ pkg }</span>
                            <a href="#" onClick={ this.clickHandler } data-user={ fake.email } className="btn btn-xs">Join As</a>
                        </div>
                        <div className="user-status">
                            <div className="status status-online"></div>
                            { fake.age }, { fake.sex } / Rome
                    	</div>
                	</div>
            	</div>
            	);
			});
		}

		return (
			<div className="chat-section">
                <div className="chat-wide">
                    { fakes }
                </div>
            </div>
		);
	}
}

export default FakeList;