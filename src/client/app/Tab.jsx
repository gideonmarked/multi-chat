import React from 'react';
import Peer from 'peerjs';
import ChatWindow from './ChatWindow.jsx';

import config from '../config/index.jsx';

class Tab extends React.Component {
	constructor(props) {
	    super(props);
	    this.state = {
	    	chatwindows: this.props.chatwindows,
	    	messages: this.props.messages,
	    	sender: this.props.sender,
	    	socket: this.props.socket,
	    	peer: new Peer(this.props.sender.id,{
		        host: config.peerurl,
		        port: config.peerport,
		        path: '/',
		        debug: 3,
		        secure: true,
		        config: {'iceServers': [
		        { url: 'stun:stun1.l.google.com:19302' },
		        { url: 'turn:numb.viagenie.ca',
		          credential: 'muazkh', username: 'webrtc@live.com' }
		        ]}
		    })
	    };

	    this.state.socket.emit('client:join', { username: this.props.sender.email });

	    this.updateMessages = this.updateMessages.bind( this );

	    var _this = this;

	    this.state.peer.on('open', function(){
	        console.log('generated', _this.state.peer.id);
	    });
	}

	componentWillReceiveProps( props ) {
		this.setState({
			messages: props.messages,
			chatwindows: props.chatwindows,
			sender: props.sender,
			socket: this.props.socket
		});
	}

	updateMessages( message ) {
		this.props.updateMessages( message );
	}

	render(){
		var chatwindows = <div />;
		if( this.state.chatwindows.length > 0 ) {
			chatwindows = this.state.chatwindows.map((chatwindow,i)=>{
				var messages = [];
				this.state.messages.map((message)=>{
					if( (message.sender.email == chatwindow[1].email && message.receiver.email == chatwindow[0].email)
						|| 
						(message.sender.email == chatwindow[0].email  && message.receiver.email == chatwindow[1].email)) {
						messages.push(message);
					}
				});
				console.log('-----' + this.state.messages + ' : ' + new Date());
				console.log('tab cw0', chatwindow[0]);
				console.log('tab cw1', chatwindow[1]);
				console.log('fltrd msgs',messages);
				return (
					<ChatWindow
					key={i}
					chatwindow={ chatwindow }
					messages={ messages }
					socket={ this.state.socket }
					peer={ this.state.peer }
					updateMessages={ this.updateMessages }
					/>
				);
			});
		}
		return (
			<div className="chat-container-bottom">
            	{ chatwindows }
          	</div>
		);
	}
}

export default Tab;