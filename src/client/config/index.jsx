'use strict';

// Settings configured here will be merged into the final config object.
export default {
  server: 'https://agnorobe.club:4008',
  peerport: '9000',
  peerurl: 'agnorobe.club',
  chat_fakes: 'https://www.betset.io/chat-fakes',
  chat_converter: 'https://www.betset.io/chat-convertor/',
  chat_people: 'https://www.betset.io/chat-people/'
}