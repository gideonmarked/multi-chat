$(document).ready(function () {
    'use strict';
    if ($('.toggle-members').length) {
        $('.toggle-members').on('click', function(e) {
            e.preventDefault();
            var form = $(this).parent().find('form');
            if (form.hasClass('hidden')) {
                form.removeClass('hidden');
            } else {
                form.addClass('hidden');
            }
        });
    }
    if ($('.members-search').length) {
        $('.members-search select').select2();
    }
});